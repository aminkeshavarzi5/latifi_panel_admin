import { createStore, applyMiddleware  } from 'redux'
import { persistCombineReducers, persistReducer } from 'redux-persist'
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import {widthReducer , authTokenReducer , businessPhoneReducer,UrlControler} from './redux/reducer'
import storage from "localforage";

const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = persistCombineReducers(persistConfig, {
  widthReducer: persistReducer(
    {
      key: "widthSize",
      storage,
      blacklist: []
    },
    widthReducer
  ),
  authTokenReducer: persistReducer(
    {
      key: "token",
      storage,
      blacklist: []
    },
    authTokenReducer
  ),
  businessPhoneReducer: persistReducer(
    {
      key: "phone",
      storage,
      blacklist: []
    },
    businessPhoneReducer
  ),
  UrlControler: persistReducer(
    {
      key: "url",
      storage,
      blacklist: []
    },
    UrlControler
  ),

});

 let persistedReducer = persistReducer(persistConfig, rootReducer)


export const store = createStore(
    persistedReducer,undefined,
    composeWithDevTools(applyMiddleware(thunk))
)
