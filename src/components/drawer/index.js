import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DehazeIcon from "@material-ui/icons/Dehaze";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import SettingsIcon from "@material-ui/icons/Settings";
import { useSelector, useDispatch } from "react-redux";
import "./style.css";
import Panel from "../Panel";
import gsap from "gsap";
import { authToken } from "../redux/actions";

export default function SwipeableTemporaryDrawer({ history }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [swiperHandle, setSwiperHandle] = useState({ left: false });
  const [section, setSection] = useState("gears");
  const width = useSelector((state) => state.widthReducer.widthSize);
  const token = useSelector((state) => state.authTokenReducer.token);

  const toggleDrawer = (anchor, open) => (event) => {
    if (event && event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }

    setSwiperHandle({ ...swiperHandle, [anchor]: open });
  };

  useEffect(() => {
    if (token === null || token === "") {
      history.push("/");
    }
  }, [token, history]);

  //exit-button-handler

  const handleExit = async () => {
    gsap.to(".right", { duration: 0.8, x: "100%" });
    gsap.to(".left", { duration: 0.8, x: "-100%", opacity: 0 });
    await setTimeout(() => {
      dispatch(authToken(null));
      history.push("/");
    }, 2000);
  };

  const list = (anchor) => (
    <div className={classes.listMobile} role="presentation" onClick={toggleDrawer(anchor, false)} onKeyDown={toggleDrawer(anchor, false)}>
      <List>
        <Button
          className={classes.btnClose}
          variant="contained"
          color="secondary"
          onClick={toggleDrawer("left", true)}
          startIcon={<ArrowForwardIosIcon style={{ color: "white", fontSize: "1.3rem" }} />}
        >
          بستن
        </Button>{" "}
        <ListItem className={classes.ListItem} onClick={() => setSection("businessman")}>
          <ListItemIcon>
            <AccountCircleIcon />
          </ListItemIcon>
          <ListItemText>پروفایل بازرگانی</ListItemText>
        </ListItem>{" "}
        <ListItem className={classes.ListItem} onClick={() => setSection("gears")}>
          <ListItemIcon>
            <SettingsIcon />
          </ListItemIcon>
          <ListItemText>ليست قطعات</ListItemText>
        </ListItem>{" "}
        <ListItem className={classes.ListItem} onClick={handleExit}>
          <ListItemIcon>
            <ExitToAppRoundedIcon />
          </ListItemIcon>
          <ListItemText>خروج</ListItemText>
        </ListItem>
      </List>
      <Divider />
    </div>
  );

  if (width > 900) {
    return (
      <div className="container">
        <div className="right">
          <List>
            {" "}
            <ListItem className={classes.ListItem} onClick={() => setSection("businessman")}>
              <ListItemIcon>
                <AccountCircleIcon />
              </ListItemIcon>
              <ListItemText>پروفایل بازرگانی</ListItemText>
            </ListItem>{" "}
            <ListItem className={classes.ListItem} onClick={() => setSection("gears")}>
              <ListItemIcon>
                <SettingsIcon />
              </ListItemIcon>
              <ListItemText>ليست قطعات</ListItemText>
            </ListItem>{" "}
            <ListItem className={classes.ListItem} onClick={handleExit}>
              <ListItemIcon>
                <ExitToAppRoundedIcon />
              </ListItemIcon>
              <ListItemText>خروج</ListItemText>
            </ListItem>
          </List>
        </div>
        <div className="left">
          <Panel history={history} content={section} />
        </div>
      </div>
    );
  } else {
    return (
      <div style={{ width: "100%", display: "flex", flexDirection: "column", background: "rgba(255,255,255,0.5)" }}>
        <DehazeIcon
          className={classes.dehazeIcon}
          style={{cursor: "pointer", margin: " 20px 20px 10px 20px", fontSize: "2.6rem" }}
          onClick={toggleDrawer("left", true)}
        />
        <div className={classes.listMobile}>
          <Panel history={history} content={section} />
        </div>
        <SwipeableDrawer anchor={"left"} open={swiperHandle["left"]} onClose={toggleDrawer("left", false)} onOpen={toggleDrawer("left", true)}>
          {list("left")}
        </SwipeableDrawer>
      </div>
    );
  }
}

const useStyles = makeStyles({
  listMobile: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0 15px",
  },
  dehazeIcon: {
    transition: "0.5s",
    color: "white",
    "&:hover": {
      transform: "scale(1.2)",
    },
  },
  btnClose: {
    transition: "0.3s",
    "&:hover": {
      color: "white",
      backgroundColor: "black",
      transform: "scale(1.1)",
    },
  },
  ListItem: {
    "&:hover": {
      backgroundColor: "rgba(231, 233, 231, 0.68)",
    },
    cursor: "pointer",
    borderRadius: 15,
  },
});
