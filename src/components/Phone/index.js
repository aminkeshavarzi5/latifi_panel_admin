import React, { useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
// import { createStyles, makeStyles } from "@material-ui/core/styles";
import "./style.css";
import PhoneNumber from "./phoneNumber";
import { useSelector } from "react-redux";
import CodeAuth from "./codeAuth";
import gsap from "gsap";

const Phone = ({ history }) => {
  const token = useSelector((state) => state.authTokenReducer.token);
  const [sms, setSms] = useState(false);

  useEffect(() => {
    const t1 = gsap.timeline();
    t1.to(".first", { duration: 1.5, top: "-100%", delay: 0.5, ease: "Expo.easeInOut" })
      .to(".second", { duration: 1.5, top: "-100%", delay: 0.7, ease: "Expo.easeInOut" }, "-=2")
      .to(".third", { duration: 1.5, top: "-100%", delay: 0.9, ease: "Expo.easeInOut" }, "-=2.3")
      .from(".content", { duration: 1, opacity: 0, y: 200, rotateZ: -360, ease: "elastic" });
  }, []);

  useEffect(() => {
    if (token) {
      history.push("/panel");
    }
  }, [token, history]);

  // useEffect(() => {
  //   if(auth === true){
  //     dispatch(authAdmin(auth));
  //     console.log("auth :", auth);
  //   }
  // }, [dispatch,auth])

  // const changeAuth = async () => {
  //   setAuth(true);
  // };

  const changeNumberToCodeAuth = () => {
    setSms(true);
  };

  const handleWrongDigit = () => {
    setSms(false);
  };

  return (
    <div className="phone container">
      <div className="content">
        <div className="title">
          <Typography style={{ margin: "30px 0 30px 0" }} variant="h3">
            پنل بازرگانان
          </Typography>
        </div>
        <div className="number-wrapper">
          {(() => {
            if (sms === false) {
              return <PhoneNumber changeNumberToCodeAuth={changeNumberToCodeAuth} />;
            } else {
              return <CodeAuth handleWrongDigit={handleWrongDigit} />;
            }
          })()}
        </div>
      </div>
      <div className="opacity" />
      <div className="overlay first" />
      <div className="overlay second" />
      <div className="overlay third" />
    </div>
  );
};

export default Phone;
