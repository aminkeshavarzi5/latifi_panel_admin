import React, { useState } from "react";
import gsap from "gsap";
import { Typography, TextField, Button } from "@material-ui/core";
import axios from "axios";
import { useDispatch } from "react-redux";
import { businessPhone } from "./../../redux/actions";
import { CircularProgress } from "@material-ui/core";
import { withSnackbar } from "notistack";
import { createStyles, makeStyles } from "@material-ui/core/styles";

const PhoneNumber = (props) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [loading, setLoading] = useState(false);

  const handleClickPhone = async (e) => {
    e.preventDefault();
    const form = e.target;
    const formData = new window.FormData(form);
    const phone = formData.get("phone");
   let PhoneWithNoZero =  phone.substring(1);
    if (phone !== "" && phone !== null) {
      await axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/loginapi/Visitor/`,
        data: {
          phone:PhoneWithNoZero,
        },
      })
        .then(async (res) => {
          if (res.data.status) {
            props.enqueueSnackbar("پیام کد تایید برای شما ارسال شد", { variant: "success" });
            setLoading(true);
            dispatch(businessPhone(res.data.phone));
            gsap.to(".number", { duration: 0.8, y: 100, opacity: 0 });
            await setTimeout(() => {
              props.changeNumberToCodeAuth();
            }, 2000);
          }
        })
        .catch((err) => {
          if (err.response.status === 500) {
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
          }
        });
    } else {
      props.enqueueSnackbar("شماره به درستی وارد نشده است", { variant: "error" });
    }
  };

  return (
    <>
      <form onSubmit={(e) => handleClickPhone(e)} className={classes.formSection}>
        {loading ? (
          <CircularProgress />
        ) : (
          <>
            <div className={classes.numberSection}>
              <Typography variant="h6" className={[classes.num, "number"].join(" ")}>
                شماره:
              </Typography>
              <TextField autoComplete="off" name="phone" className="number" placeholder="09121111111" variant="outlined" />
            </div>{" "}
            <Button style={{ fontSize: ".8rem", minWidth: 60, minHeight: 30 }} className="number" type="submit">
              ورود
            </Button>
          </>
        )}
      </form>
    </>
  );
};

const useStyles = makeStyles((theme) =>
  createStyles({
    numberSection: {
      display: "flex",
      alignItems: "center",
    },
    formSection: {
      width: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      [theme.breakpoints.down(380)]: {
        flexDirection: "column",
      },
    },
    num: {
      [theme.breakpoints.down(380)]: {
        fontSize: ".8rem",
      },
    },
  })
);

export default withSnackbar(PhoneNumber);
