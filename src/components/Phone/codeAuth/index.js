import React, { useState, useEffect } from "react";
import gsap from "gsap";
import { Typography, TextField, Button } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { authToken } from "./../../redux/actions";
import { CircularProgress } from "@material-ui/core";
import { withSnackbar } from "notistack";
import { businessPhone } from "./../../redux/actions";
import { createStyles, makeStyles } from "@material-ui/core/styles";

const CodeAuth = (props) => {
  const classes = useStyles();
  const [digit, setDigit] = useState("");
  const phone = useSelector((state) => state.businessPhoneReducer.phone);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const [resendDigit, setResendDigit] = useState(false);

  useEffect(() => {
    const dispatchToken = async () => {
      if (digit) {
        setLoading(true);
        await setTimeout(() => {
          dispatch(authToken(digit));
        }, 1500);
      }
    };
    dispatchToken();
  }, [dispatch, digit]);

  const handleClickSmsCode = (e) => {
    e.preventDefault();
    const form = e.target;
    const formData = new window.FormData(form);
    const digit = formData.get("digit");

    axios({
      method: "post",
      url: `${process.env.REACT_APP_URL}/loginapi/VisitorConfirm/`,
      data: {
        phone,
        digit,
      },
    })
      .then((res) => {
        if (res.data.status) {
          props.enqueueSnackbar("خوش آمدید", { variant: "success" });
          setDigit(res.data.Token);
          dispatch(businessPhone(res.data.Phone));
          gsap.to(".code_auth", { duration: 0.8, y: 100, opacity: 0 });
        }
      })
      .catch(async (err) => {
        props.enqueueSnackbar("کد تایید اشتباه است", { variant: "error" });
        await setTimeout(() => {
          props.handleWrongDigit();
        }, 2000);
        if (err.response.status === 500) {
          props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
        }
      });
  };

  useEffect(() => {
    let oneAndHalf = 60 * 1.2,
      display = document.querySelector("#time");
    startTimer(oneAndHalf, display);
  }, []);

  function startTimer(duration, display) {
    let timer = duration,
      minutes,
      seconds;
    let contDown = setInterval(() => {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);

      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;

      display.textContent = minutes + ":" + seconds;
      if (--timer < 0) {
        timer = duration;
      }
      if (minutes === "00" && seconds === "00") {
        clearInterval(contDown);
        setResendDigit(true);
      }
    }, 1000);
  }

  const handleResendDigit = async () => {
    await axios({
      method: "post",
      url: `${process.env.REACT_APP_URL}/loginapi/Visitor/`,
      data: {
        phone,
      },
    })
      .then(async (res) => {
        if (res.data.status) {
          props.enqueueSnackbar("پیام کد تایید برای شما ارسال شد", { variant: "success" });
          dispatch(businessPhone(res.data.phone));
          // gsap.to(".code_auth", { duration: 0.8, y: 100, opacity: 0 });
        }
      })
      .catch((err) => {
        if (err.response.status === 500) {
          props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
        }
      });
  };

  return (
    <>
      <form onSubmit={(e) => handleClickSmsCode(e)} className={classes.formSection}>
        {loading ? (
          <CircularProgress />
        ) : (
          <>
            <div className={classes.numberSection}>
              <Typography className={[classes.num, "code_auth"].join(" ")}>کد تایید:</Typography>
              <TextField autoComplete="off" name="digit" className="code_auth" placeholder="123456" variant="outlined" />
            </div>
            <Button style={{ fontSize: ".8rem", minWidth: 60, minHeight: 30 }} type="submit" className="code_auth">
              تایید
            </Button>
          </>
        )}
      </form>
      <Typography className="code_auth" style={{ marginTop: 15 }} id="time" />
      <Typography
        style={{ display: resendDigit ? "block" : "none", marginTop: 5, textAlign: "center", width: "100%", padding: 8 }}
        className="code_auth"
      >
        برای ارسال دوباره به شماره {'0'+phone} دکمه ارسال را بزنید
      </Typography>
      <Button onClick={handleResendDigit} style={{ fontSize: ".8rem", display: resendDigit ? "block" : "none", minWidth: 60, minHeight: 30 }}>
        ارسال کد
      </Button>
    </>
  );
};

const useStyles = makeStyles((theme) =>
  createStyles({
    numberSection: {
      display: "flex",
      alignItems: "center",
    },
    formSection: {
      width: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      [theme.breakpoints.down(380)]: {
        flexDirection: "column",
      },
    },
    num: {
      [theme.breakpoints.down(380)]: {
        fontSize: ".7rem",
      },
      [theme.breakpoints.down(330)]: {
        fontSize: ".6rem",
      },
    },
  })
);

export default withSnackbar(CodeAuth);
