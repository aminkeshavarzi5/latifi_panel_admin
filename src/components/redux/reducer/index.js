import { combineReducers } from 'redux'
import { createBrowserHistory } from 'history';

let resizeInitialState = {
  widthSize : window.innerWidth
}

let authTokenInitialState = {
  token : ""
}

let businessPhoneInitialState = {
  phone : ""
}

let urlPathInitialState ={
  url : createBrowserHistory().location.pathname
}



export function widthReducer(state = resizeInitialState, action) {
  switch (action.type) {
    case 'WIDTH_SIZE' : {
      return {
        widthSize : action.width 
    }
  }

    default: return state
  
    }
  }


  export function authTokenReducer(state = authTokenInitialState, action) {
    switch (action.type) {
      case 'AUTH_TOKEN' : {
        return {
          token : action.token
      }
    }
  
      default: return state
    
      }
    }

    
  export function businessPhoneReducer(state = businessPhoneInitialState, action) {
    switch (action.type) {
      case 'BUSINESS_PHONE' : {
        return {
          phone : action.phone
      }
    }
  
      default: return state
    
      }
    }

    export  const UrlControler = (state=urlPathInitialState,action) => {
      switch (action.type) {
        case 'URL_PATH': return {
          url : action.url
        }
          
        default: return state;
          
      }
    
    }




export const RootReducer = combineReducers({
  widthReducer,
  authTokenReducer,
  businessPhoneReducer,
  UrlControler,

})