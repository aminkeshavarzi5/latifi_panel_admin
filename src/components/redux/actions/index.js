

export const WidthReSize = (width) => {
  return (
   {
    type : 'WIDTH_SIZE',
    width
   }

  )
}


export const authToken = (token) => {
  return (
   {
    type : 'AUTH_TOKEN',
    token
   }

  )
}

export const businessPhone = (phone) => {
  return (
   {
    type : 'BUSINESS_PHONE',
    phone
   }

  )
}

export const updatePathname = location => {
  return {
    type: 'URL_PATH',
    url: location
  };
};
