import { unstable_createMuiStrictModeTheme as createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";
import { faIR } from '@material-ui/core/locale';

export const Theme = responsiveFontSizes(
  createMuiTheme(
    {
      direction: "rtl",
      overrides: {
        MuiCssBaseline: {
          "@global": {
            "@font-face": "iranyekan",
          },
        },
        MuiInputBase: {
          root: {
            color: "white",
          },
          input: {
            color: "white",
          },
        },
        MuiPaper: {
          root: {
            backgroundColor: "rgb(39, 34, 34)",
          },
        },
        MuiListItemIcon: {
          root: {
            minWidth: 46,
          },
        },
        MuiSvgIcon: {
          root: {
            fontSize: "1.6rem",
            color: "white",
          },
        },
        MuiList: {
          padding: {
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
          },
        },
        MuiTableCell: {
          root: {
            padding: 10,
          },
          stickyHeader: {
            backgroundColor: "black",
            color: "white",
          },
        },
        MuiButton: {
          root: {
            backgroundColor: "black",
            color: "white",
            borderRadius: 40,
            height: 35,
            margin: 10,
            minWidth: 30,
            fontSize: "1rem",
            "&:hover": {
              backgroundColor: "white",
              color: "black",
            },
          },
        },
        MuiFormControl: {
          root: {},
        },
        MuiOutlinedInput: {
          root: {
            backgroundColor: "white",
            borderRadius: 40,
            minWidth: 80,
            height: 28,
            // margin: "0 10px",
          },
          input: {
            color: "black",
            fontSize: "0.8rem",
          },
        },
        MuiInputLabel: {
          root: {
            color: "black",
            "&:hover": {
              color: "white",
            },
          },
        },
        MuiFormLabel: {
          root: {
            Mui: {
              "&::focus": {
                color: "red",
              },
            },
          },
        },
        MuiTypography: {
          root: {
            color: "white",
          },
        },
        MuiTablePagination:{
          caption:{
            direction:'initial'
          }
        },
        
        MuiSelect: {
          select: {
            color: "black",
            backgroundColor: "white",
            "&:focus": {
              color: "black",
              backgroundColor: "white",
            },
          },
        },
      },
      typography: {
        fontFamily: "iranyekan",

        h1: {
          "@media (min-width:600px)": {
            fontSize: "0.8rem",
          },
        },
      },
      palette: {
        type: "light",
        primary: {
          main: "#fff",
          dark: "#263238",
        },
        secondary: {
          main: "#080808",
        },
        error: {
          main: "#19857b",
        },
        background: {
          default: "#fff",
        },
      },
    },faIR)
);
