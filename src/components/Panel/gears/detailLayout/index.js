import React, { useState, useEffect } from "react";
import "./style.css";
import { Typography, Button, TextField } from "@material-ui/core";
import noimage from "./../assets/noimage.png";
import HighlightOffRoundedIcon from "@material-ui/icons/HighlightOffRounded";
import { makeStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import axios from "axios";
import { useSelector } from "react-redux";
import { withSnackbar } from 'notistack';
import Zoom from "react-medium-image-zoom";
import "react-medium-image-zoom/dist/styles.css";

const DetailLayout = (props) => {

  let history = props.history
  const token = useSelector((state) => state.authTokenReducer.token);
  const classes = useStyles();
  let uniqData = props.uniqData;

  const [submittedBrand, setSubmittedBrand] = useState("");
  const [submittedPrice, setSubmittedPrice] = useState("");
  const [uniqDataList, setUniqDataList] = useState({});
  const [submittedSellCondition, setSubmittedSellCondition] = useState("");
  const [openItems, setOpenItems] = useState(false);
  const handleListItems = () => {
    setOpenItems(true);
  };

  useEffect(() => {
    setUniqDataList(uniqData);
  }, [uniqData]);



  const handleRequestAdd = (e) => {
    e.preventDefault();
    const form = e.target;
    const formData = new window.FormData(form);
    const brand = formData.get("brand");
    const price = formData.get("price");
    const sellCondition = formData.get("sellCondition");
    
    if(brand !=="" && price !=="" && sellCondition !=="" ){
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/addprice/`,
        headers: {
          token,
        },
        data: {
          carpart_id: uniqDataList.id,
          brand,
          TradeType: sellCondition,
          price,
        },
      })
        .then(async(res) => {
          props.enqueueSnackbar("اطلاعات جدید اضافه شد",{variant:'success'})
          await setTimeout(() => {
            history.go('/panel')
          }, 1000);
          
          
          
        })
        .catch((err) =>{
          if(err.response.status===403){
            props.enqueueSnackbar("هر برند میتواند یک قیمت داشته باشد",{variant:'error'})
          }
          if(err.response.status===500){
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید",{variant:'info'})
          }   
        });
    }else {
      props.enqueueSnackbar("یکی از مقادیر خالی است.",{variant:'error'})
    }
   
  };

  const handleEditList = (item) => {
    let uniqItem = uniqDataList.prices[item];
    console.log('item: ',uniqItem);
    if(submittedBrand==="") {
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/changeprice/`,
        headers: {
          token,
        },
        data: {
          price_id: uniqItem.id,
          brand: uniqItem.brand,
          TradeType: submittedSellCondition,
          price: submittedPrice,
        },
      })
        .then(async(res) => {
          props.enqueueSnackbar("اطلاعات تغییر یافت",{variant:'success'})
          await setTimeout(() => {
            history.go('/panel')
          }, 1000);
        })
        .catch((err) =>{
          if(err.response.status===500){
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید",{variant:'info'})
          }  
        });
    }else if(submittedSellCondition===""){
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/changeprice/`,
        headers: {
          token,
        },
        data: {
          price_id: uniqItem.id,
          brand: uniqItem.brand,
          TradeType: uniqItem.TradeType,
          price: submittedPrice,
        },
      })
        .then(async(res) => {
          props.enqueueSnackbar("اطلاعات تغییر یافت",{variant:'success'})
          await setTimeout(() => {
            history.go('/panel')
          }, 1000);
        })
        .catch((err) =>{
          if(err.response.status===500){
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید",{variant:'info'})
          }  
        });
    }else if(submittedPrice===""){
      props.enqueueSnackbar("تغییر قیمت الزامی است.",{variant:'error'})

    }else if(submittedPrice==="" && submittedBrand==="" ){
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/changeprice/`,
        headers: {
          token,
        },
        data: {
          price_id: uniqItem.id,
          brand: uniqItem.brand,
          TradeType: submittedSellCondition,
          price: uniqItem.price,
        },
      })
        .then(async(res) => {
          props.enqueueSnackbar("اطلاعات تغییر یافت",{variant:'success'})
          await setTimeout(() => {
            history.go('/panel')
          }, 1000);
        })
        .catch((err) =>{
          if(err.response.status===500){
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید",{variant:'info'})
          }  
        });
    }else if(submittedPrice==="" && submittedBrand==="" && submittedSellCondition ==="" ){
      props.enqueueSnackbar("مقادیری را تغییر نداده اید.",{variant:'error'})
    }else{
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/changeprice/`,
        headers: {
          token,
        },
        data: {
          price_id: uniqItem.id,
          brand: submittedBrand,
          TradeType: submittedSellCondition,
          price: submittedPrice,
        },
      })
        .then(async(res) => {
          props.enqueueSnackbar("اطلاعات تغییر یافت",{variant:'success'})
          await setTimeout(() => {
            history.go('/panel')
          }, 1000);
        })
        .catch((err) =>{
          if(err.response.status===500){
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید",{variant:'info'})
          }  
        });
    }

   
  };

  const handleDeletList = (item) => {
    let uniqItem = uniqDataList.prices[item];
    axios({
      method: "post",
      url: `${process.env.REACT_APP_URL}/visitor/deletePrice/`,
      headers: {
        token,
      },
      data: {
        price_id: uniqItem.id,
      },
    })
      .then(async(res) => {
        props.enqueueSnackbar("اطلاعات مورد نظر حذف شد",{variant:'success'})
        await setTimeout(() => {
          history.go('/panel')
        }, 1000);

      })
      .catch((err) =>{
        if(err.response.status===500){
          props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید",{variant:'info'})
        } 
      });
  };

  return (
    <div className={[classes.wrapper ,"wrappers" ].join(" ")}>
      <div className="topSection">
        <div className="title">
          <Typography variant="h6">{uniqDataList.Name}</Typography>
          {uniqDataList.image ? (
            <Zoom>
            <img
              src={process.env.REACT_APP_URL + "/media/" + uniqDataList.image}
              alt={uniqDataList.Name}
              style={{ margin: 10, width: 100, height: 100, borderRadius: 15 }}
            />
            </Zoom>

          ) : (
            <img src={noimage} alt={uniqDataList.Name} style={{ margin: 10, width: 100, height: 100, borderRadius: 15 }} />
          )}
        </div>
        <div className={classes.root}>
          <form className={classes.submitedLists}>
                <ul>
                  {uniqDataList.prices ? (
                    uniqDataList.prices.map((item, index) => {
                      return (
                        <li key={item.id} className={classes.items}>
                          <span  className={classes.itemDetail}   style={{ color: "white",width:120 }}>
                            <Typography style={{margin:'0 10px'}}>برند: </Typography>
                            <TextField
                              className={classes.TextField}
                              onChange={(e) => setSubmittedBrand(e.target.value)}
                              defaultValue={item.brand}
                              autoComplete="off"
                              name="submittedBrand"
                              variant="outlined"
                            />
                          </span>
                          <span className={classes.itemDetail} style={{ color: "white",width:120 }}>
                          <Typography style={{margin:'0 10px'}}>قیمت: </Typography>
                            <TextField
                              className={classes.TextField}
                              onChange={(e) => setSubmittedPrice(e.target.value)}
                              defaultValue={item.price}
                              autoComplete="off"
                              name="submittedPrice"
                          
                              variant="outlined"
                            />
                          </span>
                          <span className={classes.itemDetail} style={{ color: "white",width:120 }}>
                          <Typography style={{margin:'0 10px'}}>شرایط فروش:  </Typography>
                            <Select
                              name="submittedSellCondition"
                              style={{ width: 83, height: 30, fontSize: ".8rem" }}
                              // id={product.id}
                              native
                              // defaultChecked="cash"
                              onChange={(e) => setSubmittedSellCondition(e.target.value)}
                              defaultValue={item.TradeType}
                            >
                              <option style={{ backgroundColor: "black", color: "white" }} value="چکی">
                                چکی
                              </option>
                              <option style={{ backgroundColor: "black", color: "white" }} value="نقدی">
                                نقدی
                              </option>
                              <option style={{ backgroundColor: "black", color: "white" }} value="شرایط ویژه">
                                شرایط ویژه
                              </option>
                            </Select>
                          </span>
                          <span className={classes.itemDetail} style={{ color: "white",width:180  }}>
                            <Button
                              onClick={(e) => handleEditList(index)}
                              style={{ fontSize: ".5rem", minWidth: 50, height: 24, color: "black", backgroundColor: "white" }}
                            >
                              ذخیره ویرایش
                            </Button>
                            <Button
                              onClick={(e) => handleDeletList(index)}
                              style={{ fontSize: ".5rem", minWidth: 50, height: 24, color: "black", backgroundColor: "white" }}
                            >
                              حذف
                            </Button>
                          </span>
                        </li>
                      );
                    })
                  ) : (
                    <li >
                      <span style={{ color: "white" }}></span>
                    </li>
                  )}
                </ul>
          </form>
        </div>
      </div>
      <div>
        <div className="add-list">
          <Typography style={{ fontSize: ".7rem" }}>برای اضافه کردن اقلام کلیک کنید</Typography>
          <div style={{ display: "flex", alignItems: "center" }}>
            <Button onClick={handleListItems} style={{ fontSize: ".5rem", minWidth: 50, backgroundColor: "white", color: "black", height: 24 }}>
              اضافه کردن
            </Button>
            <span style={{ display: openItems ? "flex" : "none" }} onClick={() => setOpenItems(false)} className={classes.closeDEtailLayout}>
              <HighlightOffRoundedIcon style={{ fontSize: "1rem" }} />
            </span>
          </div>
        </div>

        <div style={{ display: openItems ? "flex" : "none",direction: 'rtl' }} className="list-items">
          <ul>
            <li style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <form style={{ display: "flex", flexWrap: "wrap", justifyContent: "center", alignItems: "center" }} onSubmit={handleRequestAdd}>
                <TextField autoComplete="off" className={classes.TextField} placeholder="برند" name="brand" style={{ fontSize: ".8rem" }} variant="outlined" />
                <TextField autoComplete="off" className={classes.TextField} placeholder="قیمت" name="price" style={{ fontSize: ".8rem" }} variant="outlined" />

                <Select name="sellCondition" style={{ width: 90, height: 30, fontSize: ".8rem" }} native defaultValue="" id="grouped-native-select">
                  <option style={{ backgroundColor: "black", color: "white" }} value="چکی">
                    چکی
                  </option>
                  <option style={{ backgroundColor: "black", color: "white" }} value="نقدی">
                    نقدی
                  </option>
                  <option style={{ backgroundColor: "black", color: "white" }} value="شرایط ویژه">
                    شرایط ویژه
                  </option>
                </Select>

                <Button type="submit" style={{ color: "black", backgroundColor: "white", fontSize: ".6rem", width: 47, height: 26 }}>
                  ذخیره
                </Button>
              </form>
            </li>
          </ul>
        </div>
      </div>

      {props.children}
    </div>
  );
};

const useStyles = makeStyles({
  closeDEtailLayout: {
    cursor: "pointer",
    "&:hover": {
      transform: "scale(1.2)",
    },
  },
  wrapper:{
    '&::-webkit-scrollbar': {
      width:7,
   }, 
   '&::-webkit-scrollbar-track': {
     backgroundColor:'transparent',
     borderRadius:15
   }, 
   '&::-webkit-scrollbar-thumb': {
     backgroundImage: 'linear-gradient(to top, #ff0844 0%, #ffb199 100%)',
     borderRadius:15,
     "&:hover":{
       backgroundImage: 'linear-gradient(to top, #e6b980 0%, #eacda3 100%)'
     }
   }
  },
  submitedLists:{
    direction:'ltr',
  },
  root: {
    maxWidth: 606,
    margin: "0 auto",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
   
  },
  items:{
    display:'flex',
    flexWrap:'wrap',
    alignItems:'center',
    justifyContent:'center',
    borderBottom:'2px dashed white',
    maxWidth:606,
  },
  itemDetail:{
    display:'flex',
    flexWrap:'wrap',
    minWidth:250,
    maxWidth:250,
    alignItems:'center',
    justifyContent:'center',
    margin:10
  },
  TextField:{
    width:180,
    margin:'10px 0'
  }
});

export default withSnackbar(DetailLayout);
