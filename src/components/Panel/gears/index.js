import React, { useState, useEffect } from "react";
import Table from "./table";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";
import { TablePagination } from "@material-ui/core";
import { useSelector } from "react-redux";
import axios from "axios";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { withSnackbar } from "notistack";


const Gears = (props) => {

  let history = props.history;
  const classes = useStyles();
  const token = useSelector((state) => state.authTokenReducer.token);
  const [tablepage, setTablePage] = useState(0);
  const [searchpage, setSearchPage] = useState(0);
  const [dataTable, setDataTable] = useState([]);
  const [dataSearch, setDataSearch] = useState([])
  const [carPartNumbers, setcarPartNumbers] = useState(1);
  const [searchNumbers, setSearchNumbers] = useState(1);
  const [rowsPerPage] = useState(5);
  const [content, setContent] = useState(false)
  const width = useSelector((state) => state.widthReducer.widthSize);

  const handleChangeTablePage = (event, newPage) => {
    setTablePage(newPage);
  };
  const handleChangeSearchPage = (event, newPage) => {
    setSearchPage(newPage);
  };

  const handleSearch = value  => {
    if(value!==""){
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/search/`,
        headers: {
          token,
        },
        data: {
          searchChar: value,
          per_page:rowsPerPage,
          pageNumber:searchpage + 1
        },
      })
        .then((res) => {
          console.log('result :',res);
          setDataSearch(res.data.searchresult);
          setSearchNumbers(res.data.carpartNumber);
        })
        .catch((err) => {
          if (err.response.status === 500) {
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
          }
          if (err.response.status === 404) {
            setContent(true)
            setDataSearch([]);
          }
        });
    }
   
  }

  useEffect(() => {
    axios({
      method: "post",
      url: `${process.env.REACT_APP_URL}/visitor/carparts/`,
      headers: {
        token,
      },
      data: {
        pageNumber: tablepage + 1,
        per_page: rowsPerPage,
      },
    })
      .then((res) => {
        setDataTable(res.data.carparts);
        setcarPartNumbers(res.data.carpartNumber);
      })
      .catch((err) => {
        if (err.response.status === 500) {
          props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
        }
      });
  }, [token, props, tablepage, rowsPerPage]);

  const handleCloseSearch = () => {
    setContent(false)
    document.getElementById('textSearch').value=""
      ;

      
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/carparts/`,
        headers: {
          token,
        },
        data: {
          pageNumber: tablepage + 1,
          per_page: rowsPerPage,
        },
      })
        .then((res) => {
          setDataTable(res.data.carparts);
          setcarPartNumbers(res.data.carpartNumber);
        })
        .catch((err) => {
          if (err.response.status === 500) {
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
          }
        });
  }

  useEffect(() => {
   let searchBar = document.getElementById('textSearch').value
    if(searchBar!==""){
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/search/`,
        headers: {
          token,
        },
        data: {
          searchChar: searchBar,
          per_page:rowsPerPage,
          pageNumber:searchpage + 1
        },
      })
        .then((res) => {
          console.log('result :',res);
          setDataSearch(res.data.searchresult);
          setSearchNumbers(res.data.carpartNumber);
        })
        .catch((err) => {
          if (err.response.status === 500) {
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
          }
          if (err.response.status === 404) {
            setContent(true)
            setDataSearch([]);
          }
        });
    }
  }, [searchpage,props,rowsPerPage,token])

  return (
    <div className={classes.container}>
      <span className={classes.searchBar} style={{width: width < 1100 ? "90%" : "50%" , margin: width < 960 ? "0px auto 15px auto" : "20px auto"}} >
      <span style={{width:25,height:25,margin:'0 10px'}} onClick={handleCloseSearch}>
      <HighlightOffIcon className={classes.searchCloseTab} style={{display:content ? 'inline':'none'}} />
      </span>
      <TextField
        style={{ width: '100%',boxShadow:'  0px 0px 11px 1px rgba(0,0,0,0.55)',borderRadius:15}}
        placeholder="جستجو ..."
        id="textSearch"
        autoComplete="off"
        variant="outlined"
        onChange={e => handleSearch(e.target.value)}
        onFocus={() => setContent(true)}
        // onBlur={() => setContent(false)}
      />


      </span>
     
            <Table content={content} dataSearch={dataSearch}  dataTable={dataTable } history={history} />
            <TablePagination
              style={{
                margin: "10px auto",
                display: content ? 'none':'block' ,
                direction:'rtl',
                backgroundColor: "rgba(31, 31, 31, 0.9)",
                borderRadius: 40,
                color: "white",
                padding: 5,
                height: 54,
                overflow: "hidden",
              }}
              rowsPerPageOptions={[-1]}
              component="span"
              count={carPartNumbers}
              page={tablepage}
              rowsPerPage={rowsPerPage}
              onChangePage={handleChangeTablePage}
            />
             <TablePagination
              style={{
                margin: "10px auto",
                display: content ?'block':'none' ,
                direction:'rtl',
                backgroundColor: "rgba(31, 31, 31, 0.9)",
                borderRadius: 40,
                color: "white",
                padding: 5,
                height: 54,
                overflow: "hidden",
              }}
              rowsPerPageOptions={[-1]}
              component="span"
              count={searchNumbers}
              page={searchpage}
              rowsPerPage={rowsPerPage}
              onChangePage={handleChangeSearchPage}
            />
    </div>
  );
};

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      overflow: "hidden",
      width: "100%",
      padding: "1%",
      display: "flex",
      flexDirection: "column",
    },
    list: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      maxWidth: 300,
      padding: "10px 0",
    },
    searchBar:{
      display:'flex',
      alignItems:'center'
    },
    searchCloseTab:{
      transition:'all 0.5s ease',
      width:'100%',
      height:'100%',
      "&:hover": {
        transform: "scale(1.2)",
      },
    }
  })
);

export default withSnackbar(Gears);
