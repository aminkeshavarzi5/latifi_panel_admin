import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Button, Typography } from "@material-ui/core";
import Zoom from "react-medium-image-zoom";
import "react-medium-image-zoom/dist/styles.css";
import DetailLayout from "./detailLayout/";
import { CircularProgress } from "@material-ui/core";
import HighlightOffRoundedIcon from "@material-ui/icons/HighlightOffRounded";
import noimage from "./assets/noimage.png";
import { withSnackbar } from "notistack";

const columnss = [
  { id: "id", label: "شناسه", width: 80 },
  { id: "image", label: "عکس", width: 100 },
  {
    id: "name",
    label: "نام",
    width: 250,
    align: "left",
    format: (value) => value.toLocaleString(),
  },
  {
    id: "situation",
    label: "وضعیت تغییرات",
    width: 250,
    align: "left",
    format: (value) => value.toLocaleString(),
  },
];

function StickyHeadTable(props) {
  const {content,dataTable,dataSearch} = props


  useEffect(() => {
    console.log('content : ',content);
    
  }, [content])

  let history = props.history;
  const classes = useStyles();
  const [openDetail, setOpenDetail] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [dataLAyout, setDataLAyout] = useState({});



  useEffect(() => {
    if(content){
      setDataList(dataSearch);
    }else{
      setDataList(dataTable);
    }
    console.log('dataList: ',dataList);

  }, [dataTable,content,dataSearch,dataList]);

  // useEffect(() => {
  //   if(data){
  //     console.log("datalist: ",data);
  //    }

  // }, [data])

  // const handleSellConditionChange = (value) => {
  //   setSellCondition(value);
  //   console.log("sellCondition: ", value);
  // };

  const handleDetailLayout = (id) => {
    setOpenDetail(true);
    let uniqData = dataList.filter((data) => data.id === id);
    setDataLAyout(uniqData[0]);
  };

  return (
    <Paper  className={classes.root}>
      <TableContainer  className={classes.container}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow style={{ height: 70 }}>
              {columnss.map((column) => (
                <TableCell key={column.id} align={column.align} style={{ width: column.width }}>
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {dataList.length ? (
              dataList.map((product) => {
                return (
                  <TableRow key={product.id} className={classes.tableBodyRow} hover role="checkbox" tabIndex={-1}>
                    <TableCell>{product.id}</TableCell>
                    <TableCell>
                      {product.image ? (
                        <Zoom>
                          <img
                            className={classes.productImg}
                            style={{ width: 50, height: 50, borderRadius: 15, border: "2px solid white" }}
                            src={process.env.REACT_APP_URL + "/media/" + product.image}
                            alt={product.name}
                          />
                        </Zoom>
                      ) : (
                        <img
                          className={classes.productImg}
                          style={{ width: 50, height: 50, borderRadius: 15, border: "2px solid white" }}
                          src={noimage}
                          alt={product.name}
                        />
                      )}
                    </TableCell>
                    <TableCell style={{ minWidth: 200 }}>{product.Name}</TableCell>
                    <TableCell
                      style={{
                        // textAlign: "center",
                        display: "flex",
                        alignItems: "center",
                        minHeight: 77,
                      }}
                    >
                      <Button onClick={() => handleDetailLayout(product.id)} style={{ height: 24, width: 75, fontSize: "0.5rem" }}>
                        مشاهده و ذخیره
                      </Button>
                      <Typography style={{ color: "black", fontSize: "0.8rem", minWidth: 123 }}>
                        {product.prices[0] ? "اقلامی را ثبت کرده اید" : "موردی ثبت نشده است"}
                      </Typography>
                    </TableCell>
                  </TableRow>
                );
              })
            ) : (
              content ? (
                <TableRow>
                <TableCell>
                  <CircularProgress  />
                </TableCell>
                <TableCell style={{minWidth:150}}>
                  موردی یافت نشد.
                </TableCell>
              </TableRow>
              ) : (
                <TableRow>
                <TableCell>
                  <CircularProgress  />
                </TableCell>
              </TableRow>
              )
             
            )}
          </TableBody>
        </Table>
      </TableContainer>
      {openDetail === true ? (
        <div className={classes.detail} style={{ display: openDetail ? "flex" : "none" }}>
          <DetailLayout history={history} uniqData={dataLAyout}>
            <span onClick={() => setOpenDetail(false)} className={classes.closeDEtailLayout}>
              <HighlightOffRoundedIcon style={{ fontSize: "2rem" }} />
            </span>
          </DetailLayout>
        </div>
      ) : null}
    </Paper>
  );
}

const useStyles = makeStyles({
  root: {
    width: "100%",
    margin: "0 auto",
    direction:'rtl',
    maxWidth: 960,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
  },
  table:{
    direction:'ltr',
  },
  container: {
    maxHeight: "70vh",
    direction:'ltr',
    maxWidth: 960,
    overflowX:'scroll',
    "&::-webkit-scrollbar": {
      height: 7,
      width:7
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "black",
      borderRadius: 15,
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundImage: "linear-gradient(to right, #f9d423 0%, #ff4e50 100%)",
      borderRadius: 15,
      "&:hover": {
        backgroundImage: "linear-gradient(to top, #e6b980 0%, #eacda3 100%)",
      },
    },
  },
  formControl: {
    margin: 8,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: 16,
  },
  tableBodyRow: {
    position: "relative",
    "&:focus": {
      backgroundColor: "green",
    },
  },
  detail: {
    position: "fixed",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    top: "0",
    left: "0",
    background: "rgba(255,255,255,0.6)",
    zIndex: 654121548748,
  },
  closeDEtailLayout: {
    position: "absolute",
    top: 5,
    left: 5,
    cursor: "pointer",
    "&:hover": {
      transform: "scale(1.2)",
    },
  },
});

export default withSnackbar(StickyHeadTable);
