import React, { useCallback, useState, useEffect } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { Typography, TextField, Button } from "@material-ui/core";
import AddCircleRoundedIcon from "@material-ui/icons/AddCircleRounded";
import { Controlled as ControlledZoom } from "react-medium-image-zoom";
import "react-medium-image-zoom/dist/styles.css";
import { useSelector } from "react-redux";
import "./style.css";
import { withSnackbar } from "notistack";
import axios from "axios";
import noimage from "./../gears/assets/noimage.png";

const Bussinesman = (props) => {
  const token = useSelector((state) => state.authTokenReducer.token);
  const classes = useStyles();
  const [isZoomed, setIsZoomed] = useState(false);
  const [name, setName] = useState("");
  const [commerialName, setCommerialName] = useState("");
  const [picture, setPicture] = useState(undefined);
  const [pdf, setPdf] = useState(undefined);

  const handleImgChoose = () => {
    props.enqueueSnackbar("عکس انتخاب شد", { variant: "info" });
  };
  const handlePdfChoose = () => {
    props.enqueueSnackbar("پی دی اف انتخاب شد", { variant: "info" });
  };

  const handleZoomChange = useCallback((shouldZoom) => {
    setIsZoomed(shouldZoom);
  }, []);

  //getting infomation for profile
  useEffect(() => {
    axios({
      method: "post",
      url: `${process.env.REACT_APP_URL}/visitor/profile/`,
      headers: {
        token,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          setCommerialName(res.data.profile[0].commercial_name);
          setName(res.data.profile[0].name);
          setPicture(res.data.profile[0].photo);
          setPdf(res.data.profile[0].pdf);
        }
      })
      .catch((err) => {
        if (err.response.status === 500) {
          props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
        }
      });
  }, [token, props]);

  const handleRequestProfile = (e) => {
    e.preventDefault();
    const form = e.target;
    const formData = new window.FormData(form);
    const name = formData.get("edit-name");
    const commercialName = formData.get("edit-commercialName");
    const photo = formData.get("edit-image-profile");
    const pdf = formData.get("edit-pdf-profile");

    //setting varibles into form data
    formData.set("name", name);
    formData.set("commercial_name", commercialName);
    formData.append("profileImage", photo);
    formData.append("pdf", pdf);

    //calculating amount size of photo
    let fileSize = (photo.size / 1024 / 1024).toFixed(4); //file size is based on MB now
    //making request
    if (fileSize > 2) {
      props.enqueueSnackbar("حجم عکس بیش از 2 مگابایت است", { variant: "error" });
    } else {
      props.enqueueSnackbar("لطفا تا بارگزاری کامل صبر کنید", { variant: "info" });
      axios({
        method: "post",
        url: `${process.env.REACT_APP_URL}/visitor/changeprofile/`,
        headers: {
          token,
        },
        data: formData,
      })
        .then(async (res) => {
          if (res.status === 200) {
            props.enqueueSnackbar("اطلاعات شما با موفقیت ویرایش شد", { variant: "success" });
            await setTimeout(() => {
              props.history.go("/panel");
            }, 1000);
          }
        })
        .catch((err) => {
          if (err.response.status === 500) {
            props.enqueueSnackbar("خطایی رخ داده است،لطفا دوباره امتحان کنید", { variant: "info" });
          }
        });
    }
  };

  return (
    <div className={classes.container}>
      <form className={classes.div_wrapper} onSubmit={(e) => handleRequestProfile(e)}>
        <ul className={classes.ulList} style={{ minHeight: 200, height: "100%" }}>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography style={{ width: "100%", textAlign: "center", marginTop: 10, minHeight: 40 }} variant="h4">
                مشخصات
              </Typography>
            </div>
          </li>

          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary" variant="h6">
                نام:{" "}
              </Typography>
              <Typography className={classes.typo} variant="h6">
                {name !== "" ? name : "نامی ثبت نکرده اید."}
              </Typography>
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary" variant="h6">
                نام تجاری:{" "}
              </Typography>
              <Typography className={classes.typo} variant="h6">
                {commerialName !== "" ? commerialName : "نامی ثبت نکرده اید."}
              </Typography>
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary" variant="h6">
                عکس:{" "}
              </Typography>
              {picture === "" || picture === undefined ? (
                <img src={noimage} style={{ width: 60, height: 60, border: "2px black dashed", borderRadius: 15 }} alt="عکسی بارگزاری نشده است" />
              ) : (
                <ControlledZoom
                  isZoomed={isZoomed}
                  onZoomChange={handleZoomChange}
                  wrapStyle={{ width: 60, height: 60 }}
                  overlayBgColorEnd="rgba(14, 99, 190, 1)"
                >
                  <img
                    style={{ width: 60, height: 60, border: "2px black dashed", borderRadius: 15 }}
                    src={process.env.REACT_APP_URL + "/media/" + picture}
                    alt={name}
                  />{" "}
                </ControlledZoom>
              )}
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary">
                پی دی اف:{" "}
              </Typography>
              <Typography className={classes.typo}>{pdf !== "" ? pdf : "پی دی اف ثبت نکرده اید."}</Typography>
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography style={{ width: "100%", textAlign: "center", marginTop: 10, minHeight: 40 }} variant="h4">
                ویرایش اطلاعات
              </Typography>
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary" variant="h6">
                نام:{" "}
              </Typography>
              <TextField autoComplete="off" name="edit-name" style={{ color: "white" }} color="primary" id="standard-basic" placeholder={name} />
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary" variant="h6">
                نام تجاری:{" "}
              </Typography>
              <TextField autoComplete="off" name="edit-commercialName" style={{ color: "white" }} color="primary" id="standard-basic" placeholder={commerialName} />
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary" variant="h6">
                عکس:{" "}
              </Typography>
              <div className="upload-btn-wrapper">
                <AddCircleRoundedIcon style={{ fontSize: "2rem", cursor: "pointer", color: "white" }} />
                <input onChange={handleImgChoose} accept="image/png, image/jpeg, image/jpeg" type="file" name="edit-image-profile" />
              </div>
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <Typography className={classes.typo} color="primary" variant="h6">
                pdf:{" "}
              </Typography>

              <div className="upload-btn-wrapper">
                <AddCircleRoundedIcon style={{ fontSize: "2rem", cursor: "pointer", color: "white" }} />
                <input onChange={handlePdfChoose} type="file" accept=".pdf" name="edit-pdf-profile" />
              </div>
            </div>
          </li>
          <li className={classes.list}>
            <div className={classes.wrapperList}>
              <span style={{ width: "100%", display: "flex", justifyContent: "center", flexDirection: "column", padding: 10 }}>
                <Typography
                  className={classes.typo}
                >
                  برای ویرایش اطلاعات،کافی است اطلاعات جدید را وارد و روی دکمه ویرایش بزنید.
                </Typography>
                <Button type="submit" style={{ fontSize: "0.8rem", margin: "0 auto" }}>
                  ویرایش
                </Button>
              </span>
            </div>
          </li>
        </ul>
      </form>
    </div>
  );
};

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      overflow: "hidden",
      width: "100%",
      padding: "15px 5% 5% 5%",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    div_wrapper: {
      display: "flex",
      border: "3px white solid",
      backgroundImage: "linear-gradient(to top, #ff0844 0%, #ffb199 100%)",
      borderRadius: 10,
      flexDirection: "column",
      maxHeight: 400,
      marginTop: 10,
      position: "relative",
      width: "100%",
      maxWidth: 750,
      overflowY: "auto",
      direction: "rtl",
      [theme.breakpoints.down(1572)]: {
        width: "100%",
      },
      "&::-webkit-scrollbar": {
        width: 10,
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "transparent",

        borderRadius: 15,
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "black",
        borderRadius: 15,
        "&:hover": {
          backgroundImage: "linear-gradient(to top, #e6b980 0%, #eacda3 100%)",
        },
      },
    },
    ulList: {
      display: "flex",
      direction: "ltr",
      flexWrap: "wrap",
      justifyContent: "space-around",
      marginTop: 10,
      position: "relative",
      width: "100%",
      height: "50%",
    },
    list: {
      display: "flex",
      justifyContent: "space-around",
      flexDirection: "column",
      alignItems: "center",
      width: 300,
      margin: "0px 5%",
      overflow: "visible",
      padding: "10px 0",
    },
    wrapperList: {
      width: "100%",
      height: "100%",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    imageContainer: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: 150,
      maxWidth: 200,
      border: "2px dashed white",
      marginTop: 10,
      borderRadius: 15,
      position: "relative",
      padding: 5,
      overflow: "visible",
    },
    pdfContainer: {
      display: "flex",
      position: "relative",
      justifyContent: "center",
      alignItems: "center",
      padding: 10,
      overflow: "visible",
      marginTop: 10,
      height: 50,
      width: 200,
      borderRadius: 15,
      border: "2px solid white",
    },
    closeTab: {
      position: "absolute",
      cursor: "pointer",
      top: -14,
      left: -13,
      zIndex: 999999,
    },
    typo: {
      [theme.breakpoints.down(390)]: {
        fontSize: ".8rem",
      },
    },
  })
);

export default withSnackbar(Bussinesman);
