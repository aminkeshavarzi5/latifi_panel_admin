import React from 'react'
import Bussinesman from './bussinessman/'
import Gears from './gears/'


const Panel = (props) => {
  let history = props.history
  
  
  if(props.content === "businessman") {
  return (
    <div className="container">
      <Bussinesman history={history}/>
    </div>
  )
  }else if(props.content === "gears") {
    return(
    <div>
      <Gears history={history}/>
    </div>
    )
  }
}

export default Panel
