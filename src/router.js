import React, { useEffect } from "react";
import { Route, Switch } from "react-router-dom";
// import Loadable from "react-loadable";
// import CircularProgress from '@material-ui/core/CircularProgress';
import "./style.css";
import { WidthReSize, updatePathname } from "./components/redux/actions";
import { useDispatch } from "react-redux";
import Phone from "./components/Phone/";
import Drawer from "./components/drawer/";

const Routes = ({ history }) => {
  document.dir = "rtl";
  const dispatch = useDispatch();
  useEffect(() => {
    if (history) {
      history.listen((location) => {
        dispatch(updatePathname(location.pathname));
      });
    }

    window.addEventListener("resize", () => {
      dispatch(WidthReSize(window.innerWidth));
    });
  }, [dispatch, history]);

  return (
    <>
      <div className="wrraper">
        <Switch>
          <Route path="/" exact component={Phone} />
          <Route path="/panel" component={Drawer} />
        </Switch>
      </div>
    </>
  );
};

export default Routes;
