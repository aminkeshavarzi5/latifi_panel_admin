import React  from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import { store } from "./components/configureStore";
import { persistStore } from "redux-persist";
import { ThemeProvider, CssBaseline } from "@material-ui/core";
import { StylesProvider, jssPreset } from "@material-ui/core/styles";
import { create } from "jss";
import rtl from "jss-rtl";
import { Theme } from "./components/Mui";
import { BrowserRouter as Router } from 'react-router-dom'
import Routes from './router'
import "./iranyekan.css";
import { SnackbarProvider } from 'notistack';
import "./style.css";



const jss = create({
  plugins: [...jssPreset().plugins, rtl()],
});
const persistor = persistStore(store, {});




ReactDOM.render(
  <React.StrictMode>
    <StylesProvider jss={jss}>
      <ThemeProvider theme={Theme}>
      <SnackbarProvider maxSnack={3}>
        <CssBaseline />
        <Provider store={store}>
          <PersistGate loading={<div>loading...</div>} persistor={persistor}>
            <Router>
            <Routes />
            </Router>
          </PersistGate>
        </Provider>
        </SnackbarProvider>
      </ThemeProvider>
    </StylesProvider>
 </React.StrictMode>

  ,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
